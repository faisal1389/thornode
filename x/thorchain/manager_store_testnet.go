// +build testnet

package thorchain

import (
	"gitlab.com/thorchain/thornode/common"
	"gitlab.com/thorchain/thornode/common/cosmos"
	"gitlab.com/thorchain/thornode/constants"
)

func (smgr *StoreMgr) creditAssetBackToVaultAndPool(ctx cosmos.Context) {

}
func (smgr *StoreMgr) purgeETHOutboundQueue(ctx cosmos.Context, constantAccessor constants.ConstantValues) {

}
func (smgr *StoreMgr) correctAsgardVaultBalanceV61(ctx cosmos.Context, asgardPubKey common.PubKey) {

}
